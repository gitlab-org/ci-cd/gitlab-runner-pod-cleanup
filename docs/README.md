# GitLab Runner Pod Cleanup advanced use

By default, [`pod-cleanup.yml`](../pod-cleanup.yml):

- Uses the `latest` tag of the Docker image that has GitLab Runner Pod Cleanup.
- Creates the relevant `Role`, `RoleBinding`, and `ServiceAccount`.
- Uses the default config values from [Application Configuration](../README.md#application-configuration).

You can download and modify the [`pod-cleanup.yml`](../pod-cleanup.yml) file:

```shell
curl -s https://gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/-/raw/main/pod-cleanup.yml > pod-cleanup.yml
```

## Versions and tags

If you use the [`pod-cleanup.yml`](../pod-cleanup.yml) file from the `main` branch, you will get the `latest` Docker tag. Tags are created in the following scenarios:

- `COMMIT_SHA`: Created for each commit. Very unstable, and used mostly for development purposes.
- `latest`: Updated for each commit in the `main` branch. The `latest` tag might potentially be unstable.
- `v.X.X.X` (for example, `v1.0.0`): Created for each official release of the Pod Cleanup tool. To view available versions, browse [the tags](https://gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/-/tags). If you use [pod-cleanup.yml](../pod-cleanup.yml) from a tag, you get the correctly tagged Docker image version. For example, if you use `https://gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/-/raw/v1.0.0/pod-cleanup.yml`, it pulls the `registry.gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup:v1.0.0` image.

To manually change the Pod Cleanup version used when deploying, change the image in [`pod-cleanup.yml`](../pod-cleanup.yml) as follows:

```diff
diff --git a/pod-cleanup.yml b/pod-cleanup.yml
index 1a096b3..b6af549 100644
--- a/pod-cleanup.yml
+++ b/pod-cleanup.yml
@@ -37,4 +37,4 @@ spec:
   serviceAccountName: pod-cleanup-sa
   containers:
   - name: gitlab-runner-pod-cleanup
-    image: registry.gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup:latest
\ No newline at end of file
+    image: registry.gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup:v1.0.0
\ No newline at end of file
```

## Service accounts and authentication

### Service accounts

By default, GitLab Runner Pod Cleanup uses the created service account, `pod-cleanup-sa`, to authenticate with the Kubernetes cluster API. To use a different service account, change it in the pod definition:

```diff
diff --git a/pod-cleanup.yml b/pod-cleanup.yml
index 1a096b3..2963f41 100644
--- a/pod-cleanup.yml
+++ b/pod-cleanup.yml
@@ -34,7 +34,7 @@ metadata:
   name: gitlab-runner-pod-cleanup
 spec:
   restartPolicy: Always
-  serviceAccountName: pod-cleanup-sa
+  serviceAccountName: my-sa
   containers:
   - name: gitlab-runner-pod-cleanup
     image: registry.gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup:latest
\ No newline at end of file
```

The permissions GitLab Runner Pod Cleanup needs are:

```yaml
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["get", "list", "delete"]
```

### Alternative authentication methods

Alternatively, GitLab Runner Pod Cleanup can authenticate by using the `KUBECONFIG` that's also used by `kubectl`. This allows you to run GitLab Runner Pod Cleanup outside of a Kubernetes cluster.

## Configuration

GitLab Runner Pod Cleanup is configured only through the environment. To change the [default configuration options](../README.md#application-configuration), add environment variables to the pod definition. For example, to change the default `POD_CLEANUP_INTERVAL`:

```diff
diff --git a/pod-cleanup.yml b/pod-cleanup.yml
index 1a096b3..9fc0a16 100644
--- a/pod-cleanup.yml
+++ b/pod-cleanup.yml
@@ -37,4 +37,7 @@ spec:
   serviceAccountName: pod-cleanup-sa
   containers:
   - name: gitlab-runner-pod-cleanup
-    image: registry.gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup:latest
\ No newline at end of file
+    image: registry.gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup:latest
+    env:
+    - name: POD_CLEANUP_INTERVAL
+      value: "30s"
\ No newline at end of file
```
