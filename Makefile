VERSION ?= $(shell (scripts/version 2>/dev/null || echo "dev") | sed -e 's/^v//g')
REVISION ?= $(shell git rev-parse --short HEAD || echo "unknown")
BRANCH ?= $(shell git show-ref | grep "$(REVISION)" | grep -v HEAD | awk '{print $$2}' | sed 's|refs/remotes/origin/||' | sed 's|refs/heads/||' | sort | head -n 1)
BUILT ?= $(shell date -u +%Y-%m-%dT%H:%M:%S%z)
BUILD_PLATFORMS ?= -osarch 'linux/amd64' \
					-osarch 'darwin/amd64'

GITLAB_CHANGELOG_VERSION ?= latest
GITLAB_CHANGELOG = .tmp/gitlab-changelog-$(GITLAB_CHANGELOG_VERSION)

PKG := $(shell go list .)

GO_LDFLAGS := -X $(PKG).VERSION=$(VERSION) \
              -X $(PKG).REVISION=$(REVISION) \
              -X $(PKG).BRANCH=$(BRANCH) \
              -X $(PKG).BUILT=$(BUILT) \
              -s -w

CI_REGISTRY ?= registry.gitlab.com
CI_REGISTRY_NAMESPACE ?= gitlab-org
CI_IMAGE ?= $(CI_REGISTRY_IMAGE/ci):latest

generate_changelog: export CHANGELOG_RELEASE ?= $(VERSION)
generate_changelog: $(GITLAB_CHANGELOG)
	# Generating new changelog entries
	@$(GITLAB_CHANGELOG) -project-id 31636488 \
		-release $(CHANGELOG_RELEASE) \
		-starting-point-matcher "v[0-9]*.[0-9]*.[0-9]*" \
		-config-file .gitlab/changelog.yml \
		-changelog-file CHANGELOG.md

check-tags-in-changelog:
	# Looking for tags in CHANGELOG
	@git status | grep "On branch main" 2>&1 >/dev/null || echo "Check should be done on main branch only. Skipping."
	@for tag in $$(git tag | grep -E "^v[0-9]+\.[0-9]+\.[0-9]+$$" | sed 's|v||' | sort -g); do \
		state="MISSING"; \
		grep "^v $$tag" CHANGELOG.md 2>&1 >/dev/null; \
		[ "$$?" -eq 1 ] || state="OK"; \
		echo "$$tag:   \t $$state"; \
	done

.PHONY: compile
compile: GOOS ?=
compile: GOARCH ?=
compile:
	GOOS=$(GOOS) GOARCH=$(GOARCH) go build \
			-o build/pod-cleanup \
			-ldflags "$(GO_LDFLAGS)" .

.PHONY: test
test: GOFLAGS ?=
test:
	go test ./... -v $(GOFLAGS)

.PHONY: test_race
test_race:
	GOFLAGS=-race $(MAKE) test

.PHONY: test_integration
test_integration: GOFLAGS ?=
test_integration:
	go test ./app -tags=integration -v $(GOFLAGS)

.PHONY: lint
lint: TOOL_VERSION ?= 1.27.0
lint: OUT_FORMAT ?= colored-line-number
lint: LINT_FLAGS ?=
lint:
	@if command -v golangci-lint > /dev/null; then \
		if ! $$(golangci-lint --version | grep "version $(TOOL_VERSION)" >/dev/null); then \
		    echo "WARNING: The installed version of golangci-lint is not the recommended version $(TOOL_VERSION)!"; \
		    golangci-lint --version; \
		fi; \
		golangci-lint run ./... --out-format $(OUT_FORMAT) $(LINT_FLAGS); \
	else \
	    echo "golangci-lint does not seem to be installed. Please install it at https://github.com/golangci/golangci-lint#install"; \
	fi

.PHONY: build_ci_image
build_ci_image: GO_VERSION ?= 1.21.1
build_ci_image: GO_SHASUM ?= b3075ae1ce5dab85f89bc7905d1632de23ca196bd8336afd93fa97434cfa55ae
build_ci_image: DOCKER_VERSION ?= 24.0.6
build_ci_image: KUBECTL_VERSION ?= 1.27.5
build_ci_image:
	# Building the $(CI_IMAGE) image
	@docker build \
		--pull \
		--no-cache \
		--build-arg GO_VERSION=$(GO_VERSION) \
		--build-arg GO_SHASUM=$(GO_SHASUM) \
		--build-arg DOCKER_VERSION=$(DOCKER_VERSION) \
		--build-arg KUBECTL_VERSION="${KUBECTL_VERSION}" \
		-t $(CI_IMAGE) \
		-f dockerfiles/ci/Dockerfile \
		dockerfiles/ci/

$(GITLAB_CHANGELOG): OS_TYPE ?= $(shell uname -s | tr '[:upper:]' '[:lower:]')
$(GITLAB_CHANGELOG): DOWNLOAD_URL = "https://storage.googleapis.com/gitlab-runner-tools/gitlab-changelog/$(GITLAB_CHANGELOG_VERSION)/gitlab-changelog-$(OS_TYPE)-amd64"
$(GITLAB_CHANGELOG):
	# Installing $(DOWNLOAD_URL) as $(GITLAB_CHANGELOG)
	@mkdir -p $(shell dirname $(GITLAB_CHANGELOG))
	@curl -sL "$(DOWNLOAD_URL)" -o "$(GITLAB_CHANGELOG)"
	@chmod +x "$(GITLAB_CHANGELOG)"

